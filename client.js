const
    range = document.getElementById('range'),
    rangeV = document.getElementById('rangeV'),
    setValue = ()=>{
        const
        newValue = Number( (range.value - range.min) * 100 / (range.max - range.min) ),
        newPosition = 10 - (newValue*0.3);
        rangeV.innerHTML = `<span>${range.value}</span>`;
        rangeV.style.left = `calc(${newValue}% + (${newPosition}px))`;
        board.cells = [];
        board.size = range.value;
        board.draw(board.size);
    };
    var instructionsOverlay = document.getElementById("instructions");
    var startScreen = document.getElementById("startScreenWrapper");
    var gameField = document.getElementById("gameField");
    var addPlayer = document.getElementById("addPlayer");
    var removePlayer = document.getElementById("removePlayer");
    var startGame = document.getElementById("startGame");
    var playerOne = document.getElementById("playerOneWrapper");
    var playerOneName = document.querySelector("form[name='playerOneForm'] input[name='name']");
    var playerOneWord = document.querySelector("form[name='playerOneForm'] input[name='word']");
    var playerTwo = document.getElementById("playerTwoWrapper");
    var playerTwoName = document.querySelector("form[name='playerTwoForm'] input[name='name']");
    var playerTwoWord = document.querySelector("form[name='playerTwoForm'] input[name='word']");
    var playerThree = document.getElementById("playerThreeWrapper");
    var playerThreeName = document.querySelector("form[name='playerThreeForm'] input[name='name']");
    var playerThreeWord = document.querySelector("form[name='playerThreeForm'] input[name='word']");
    var playerFour = document.getElementById("playerFourWrapper");
    var playerFourName = document.querySelector("form[name='playerFourForm'] input[name='name']");
    var playerFourWord = document.querySelector("form[name='playerFourForm'] input[name='word']");
    var counter = 0;
document.addEventListener("DOMContentLoaded", ()=>{
    instructionsOverlay.addEventListener("click", ()=>{
        instructionsOverlay.style.display = "none";
    });
    addPlayer.addEventListener("click", ()=>{
        if(counter == 0){
            playerOne.style.display = "flex";
            counter++;
        }
        else if(counter == 1){
            playerTwo.style.display = "flex";
            counter++;
        }
        else if(counter == 2){
            playerThree.style.display = "flex";
            counter++;
        }
        else if(counter == 3){
            playerFour.style.display = "flex";
            counter++; 
        }
    });
    removePlayer.addEventListener("click", ()=>{

        if(counter == 1){
            playerOne.style.display = "none";
            playerOneName.value = '';
            playerOneWord.value = '';
            counter--;
        }
        else if(counter == 2){
            playerTwo.style.display = "none";
            playerTwoName.value = '';
            playerTwoWord.value = '';
            counter--;
        }
        else if(counter == 3){
            playerThree.style.display = "none";
            playerThreeName.value = '';
            playerThreeWord.value = '';
            counter--;
        }
        else if(counter == 4){
            playerFour.style.display = "none";
            playerFourName.value = '';
            playerFourWord.value = '';
            counter--; 
        }
    });
    setValue();
});
range.addEventListener('input', setValue);

function hexToRgbA(hex){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(', ')+', 0.5)';
    }
    throw new Error('Bad Hex');
}