const canvas = document.querySelector('canvas');
canvas.height = innerHeight;
canvas.width = innerWidth;
const ctx = canvas.getContext("2d");
var Fonts = ["Hamburger","Marker", "Rival"];
var PlayerColors = ["#6a9c2e", "#714aa2", "#a71260", "#dc6d02"];
const
range = document.getElementById('range'),
rangeV = document.getElementById('rangeV'),
setValue = ()=>{
    const
    newValue = Number( (range.value - range.min) * 100 / (range.max - range.min) ),
    newPosition = 10 - (newValue*0.3);
    rangeV.innerHTML = `<span>${range.value}</span>`;
    rangeV.style.left = `calc(${newValue}% + (${newPosition}px))`;
    board.cells = [];
    board.size = range.value;
    board.draw(board.size);
};
var instructionsOverlay = document.getElementById("instructions");
var startScreen = document.getElementById("startScreenWrapper");
var gameField = document.getElementById("gameField");
var addPlayerFrontend = document.getElementById("addPlayer");
var removePlayer = document.getElementById("removePlayer");
var startGame = document.getElementById("startGame");
var playerOne;
var playerTwo;
var playerThree;
var playerFour;
var playerCounter = 0;

class PlayerFrontEnd {
    constructor(_number){
        this.wrapper = document.getElementById("player"+_number+"Wrapper");
        this.name = document.querySelector("form[name='player"+_number+"Form'] input[name='name']");
        this.word = document.querySelector("form[name='player"+_number+"Form'] input[name='word']");
        this.cpu = document.querySelector("form[name='player"+_number+"Form'] input[name='cpu']");
    }
}
class Board {
    constructor(size){
        this.size = size;
        this.players = [];
        this.currentPlayer = null;
        this.previousPlayer = null;
        this.cells = [];
        this.winner = null;
    }
    draw(){
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        this.cells = [];
        let cellSize = (canvas.height-100)/this.size;
        let offsetX = innerWidth/2 - cellSize*this.size/2;
        let offsetY = innerHeight/2 - cellSize*this.size/2;
        let index = 0;
        ctx.fillStyle = "#c0c0c0";
        ctx.fillRect(offsetX-2,offsetY-2,this.size*cellSize+4,this.size*cellSize+4);
        for (let row = 0; row < this.size; row++) {
            for (let column = 0; column < this.size; column++) {
                this.cells.push(new Cell(offsetX,offsetY,cellSize,index,row,column,null));
                this.cells[index].draw(); 
                index++;
                offsetX += cellSize;
            }
            offsetX = innerWidth/2 - cellSize*this.size/2; 
            offsetY += cellSize;
        }
    }
    addPlayer(player){
        this.players.push(player);
    }
    detectWin(){
        let pomArray = [];
        for (let index = 0; index < this.cells.length; index++) {
            pomArray.push(this.cells[index].value);   
        }
        this.players.forEach((player) => {
            if(pomArray.toString().includes(player.word.toString()) || pomArray.reverse().toString().includes(player.word.toString()))
            {
                console.log(`Igrač ${player.id + 1} je pobednik.`);
                this.winner = player;
            }
        });
        pomArray = [];
        for (let index = 0; index < this.size; index++) {
            this.cells.forEach(cell => {
                if(cell.column == index){
                    pomArray.push(cell.value);  
                } 
            });
            this.players.forEach((player) => {
                if(pomArray.toString().includes(player.word.toString()) || pomArray.reverse().toString().includes(player.word.toString()))
                {
                    console.log(`Igrač ${player.id + 1} je pobednik.`);
                    this.winner = player;
                }
            });
            pomArray = [];
        }
    }
    manageTurns(){
        if(this.winner == null){
            if(this.currentPlayer.turn == this.currentPlayer.word.length-1)
                this.currentPlayer.turn = 0;
            else
                this.currentPlayer.turn++;
            if(this.players[this.currentPlayer.id + 1]){
                this.previousPlayer = this.currentPlayer;
                this.currentPlayer = this.players[this.currentPlayer.id + 1]
            }   
            else{
                this.previousPlayer = this.currentPlayer;
                this.currentPlayer = this.players[0];
            }    
            if(this.currentPlayer.cpu){
                this.currentPlayer.playTurn(this);
                canvas.style.cursor = "wait";
                canvas.style.pointerEvents = "none";
            }
            else{
                canvas.style.cursor = "pointer";
                canvas.style.pointerEvents = "auto";
            }
        }   
    }
}

class Cell {
    constructor(_x,_y,_size,_id,_row,_column,_value){
        this.x = _x;
        this.y = _y;
        this.size = _size;
        this.id = _id;
        this.value = _value;
        this.row = _row;
        this.column = _column;
        this.leterShown = false;
    }
    draw(){
        ctx.fillStyle = "#c0c0c0";
        ctx.fillRect(this.x,this.y,this.size,this.size);
        ctx.fillStyle = "#fbfbf8";
        ctx.fillRect(this.x+1,this.y+1,this.size-2,this.size-2);
    }
    addLetter(letter){
        this.value = letter;
        ctx.font = ""+this.size/1.5+"px "+Fonts[board.currentPlayer.id]+"";
        ctx.fillStyle = board.currentPlayer.color;
        ctx.fillText(letter, this.x + this.size/2/1.5, this.y + this.size/1.3);
    }
    showLetter(letter){
        ctx.font = ""+this.size/1.5+"px "+Fonts[board.currentPlayer.id]+"";
        ctx.fillStyle = hexToRgbA(board.currentPlayer.color);
        ctx.fillText(letter, this.x + this.size/2/1.5, this.y + this.size/1.3);
        this.leterShown = true;
    }
    hideLetter(){
        board.cells[this.id].draw();
        this.leterShown = false;
    }
}



class Player {
    constructor(_id, _color, _word){
        this.id = _id;
        this.color = _color;
        this.word = _word.toUpperCase().split('');
        this.turn = 0;
        this.cpu = false;
        this.path = null;
    }
    detectPath(board, cell){
        let rightScope = board.cells.slice(cell.id - this.turn, cell.id + this.word.length - this.turn);
        let leftScope = board.cells.slice(cell.id + 1 - this.word.length + this.turn, cell.id + 1 + this.turn);
        let topScope = [];
        let bottomScope = [];
        for (let index = 0; index < this.word.length; index++) {
            if(index == 0)
                topScope.push(board.cells[cell.id+this.turn*board.size]);
            else
                topScope.push(board.cells[(cell.id+this.turn*board.size) - index*board.size])
        }
        for (let index = 0; index < this.word.length; index++) {
            if(index == 0)
                bottomScope.push(board.cells[cell.id-this.turn*board.size]);
            else
                bottomScope.push(board.cells[(cell.id-this.turn*board.size) + index*board.size])
        }
        if(rightScope.slice(0, this.word.length-1).every((element, index)=>{return element.value == this.word[index]})){
            this.path = rightScope;
            return true;
        }   
        else if(leftScope.reverse().slice(0, this.word.length-1).every((element, index)=>{return element.value == this.word[index]})){
            this.path = leftScope;
            return true;
        }   
        else if(bottomScope.slice(0, this.word.length-1).every((element, index)=>{return element.value == this.word[index]})){
            this.path = bottomScope;
            return true;
        }    
        else if(topScope.slice(0, this.word.length-1).every((element, index)=>{return element.value == this.word[index]})){
            this.path = topScope;
            return true;
        }    
        else{
            this.path = null;
            return false;
        }
            
    }
}

class PlayerCPU extends Player{
    constructor(_id, _color, _word){
        super(_id, _color, _word);
        this.cpu = true;
    }
    playTurn(board){
        setTimeout(function(){
            if(blockOthers(board)) 
                addToPath(board);
            board.detectWin();
            board.manageTurns();
        }, (Math.floor(Math.random() * 2) + 1) * 1000);
        function addToPath(board){
            if(board.currentPlayer.path == null){
                board.currentPlayer.path = determinePath(board);
            }
            if(board.currentPlayer.path[board.currentPlayer.turn].value != null && board.currentPlayer.path[board.currentPlayer.turn].value == board.currentPlayer.word[board.currentPlayer.turn])
                randomCell(board).addLetter(board.currentPlayer.word[board.currentPlayer.turn]);
            else if (board.currentPlayer.path[board.currentPlayer.turn].value != null){
                board.currentPlayer.path = determinePath(board);
                board.currentPlayer.path[board.currentPlayer.turn].addLetter(board.currentPlayer.word[board.currentPlayer.turn]);
            }
            else
                board.currentPlayer.path[board.currentPlayer.turn].addLetter(board.currentPlayer.word[board.currentPlayer.turn]);   
        
            function determinePath(board){
                let cell = randomCell(board);
                let leftScope = [];
                let rightScope = [];
                let topScope = [];
                let bottomScope = [];
                let pomArray = [];
                
                leftScope = board.cells.slice(cell.id - board.currentPlayer.word.length + 1,cell.id + 1).reverse();
                rightScope = board.cells.slice(cell.id,cell.id + board.currentPlayer.word.length);
                board.cells.forEach((element)=>{if(element.column == cell.column)pomArray.push(element)});
                bottomScope = pomArray.slice(pomArray.indexOf(cell), pomArray.indexOf(cell) + board.currentPlayer.word.length)
                topScope = pomArray.slice(pomArray.indexOf(cell) - board.currentPlayer.word.length + 1, pomArray.indexOf(cell) + 1).reverse();

                if(leftScope.length == board.currentPlayer.word.length && leftScope.every((element,index)=>{return element.value == null && element.row == leftScope[0].row || element.value == board.currentPlayer.word[index] && element.row == leftScope[0].row})){
                    return leftScope;
                }   
                else if(rightScope.length == board.currentPlayer.word.length && rightScope.every((element,index)=>{return element.value == null && element.row == rightScope[0].row || element.value == board.currentPlayer.word[index] && element.row == rightScope[0].row})){
                    return rightScope;
                }    
                else if(bottomScope.length == board.currentPlayer.word.length && bottomScope.every((element,index)=>{return element.value == null && element.column == bottomScope[0].column || element.value == board.currentPlayer.word[index] && element.column == bottomScope[0].column})){
                    return bottomScope;
                }   
                else if(topScope.length == board.currentPlayer.word.length && topScope.every((element,index)=>{return element.value == null && element.column == topScope[0].column || element.value == board.currentPlayer.word[index] && element.column == topScope[0].column})){
                    return topScope;
                }
                else{
                    return determinePath(board);
                } 
            }
            function randomCell(board){
                let min = Math.ceil(0);
                let max = Math.floor(board.cells.length-1);
                let cell = board.cells[Math.floor(Math.random() * (max - min + 1)) + min];
                if(cell.value != null){
                    return randomCell(board);
                }   
                else
                    return cell;
            }
        }
        /*function blockWord(board){
            if(board.previousPlayer != null){
                let row = [];
                let column = [];
                let matchesRow = [];
                let matchesRowReverse = [];
                let matchesColumn = [];
                let matchesColumnReverse = [];
                let rowSlice = [];
                let rowSliceReverse = [];
                let columnSlice = [];
                let columnSliceReverse = [];

                for (let index = 0; index <= board.size - 1; index++) {
                    board.cells.forEach((element)=>{if(element.row == index)row.push(element)});
                    board.cells.forEach((element)=>{if(element.column == index)column.push(element)});
                    for (let index = 0; index <= row.length - board.previousPlayer.word.length; index++){
                        rowSlice = row.slice(index, index + board.previousPlayer.word.length);
                        rowSlice.forEach((element, index)=>{if(element.value == board.previousPlayer.word[index] && rowSlice.find(k => k.value==null))matchesRow.push(element)})
                        if(matchesRow.length == board.previousPlayer.word.length - 1){
                            row.slice(index, index + board.previousPlayer.word.length).find(element=>element.value == null)
                            .addLetter(board.currentPlayer.word[board.currentPlayer.turn]);
                            return true;
                        }
                        matchesRow = [];
                        rowSliceReverse = row.reverse().slice(index, index + board.previousPlayer.word.length);
                        rowSliceReverse.forEach((element, index)=>{if(element.value == board.previousPlayer.word[index] && rowSliceReverse.reverse().find(k => k.value==null))matchesRowReverse.push(element)})
                        if(matchesRowReverse.length == board.previousPlayer.word.length - 1){
                            row.reverse().slice(index, index + board.previousPlayer.word.length).find(element=>element.value == null)
                            .addLetter(board.currentPlayer.word[board.currentPlayer.turn]);
                            return true;
                        }
                        matchesRowReverse = [];
                        columnSlice = column.slice(index, index + board.previousPlayer.word.length);
                        columnSlice.forEach((element, index)=>{if(element.value == board.previousPlayer.word[index] && columnSlice.find(k => k.value==null))matchesColumn.push(element)})
                        if(matchesColumn.length == board.previousPlayer.word.length - 1){
                            column.slice(index, index + board.previousPlayer.word.length).find(element=>element.value == null)
                            .addLetter(board.currentPlayer.word[board.currentPlayer.turn]);
                            return true;
                        }
                        matchesColumn = [];
                        columnSliceReverse = column.reverse().slice(index, index + board.previousPlayer.word.length);
                        columnSliceReverse.forEach((element, index)=>{if(element.value == board.previousPlayer.word[index] && columnSliceReverse.reverse().find(k => k.value==null))matchesColumnReverse.push(element)})
                        if(matchesColumnReverse.length == board.previousPlayer.word.length - 1){
                            column.reverse().slice(index, index + board.previousPlayer.word.length).find(element=>element.value == null)
                            .addLetter(board.currentPlayer.word[board.currentPlayer.turn]);
                            return true;
                        }
                        matchesColumnReverse = [];
                    }
                    row = [];
                    column = [];
                }
            }
            return false;
        }*/
        function blockOthers(board){
            let result = board.players.every(player=>{
                if(player != board.currentPlayer 
                    && player.path != null && player.path.find(element=>element.value == null) != undefined 
                    && player.path.filter((element, index)=>{return element.value == player.word[index]}).length == 4)
                {
                    player.path.find(element=>element.value == null).addLetter(board.currentPlayer.word[board.currentPlayer.turn]);
                    return false;
                }
                return true;
            });
            return result;
        }
    }
}

canvas.addEventListener('click', function(e) {
    var x = e.pageX - canvas.offsetLeft;
    var y = e.pageY - canvas.offsetTop;
    board.cells.forEach(element => {
        if(element.x < x && x < element.x + element.size && element.y < y && y < element.y + element.size){
            if(element.value == null && !board.currentPlayer.cpu){
                element.addLetter(board.currentPlayer.word[board.currentPlayer.turn]);
                board.currentPlayer.detectPath(board, element);
                board.detectWin();
                board.manageTurns();
            }  
        }
    });
});

canvas.addEventListener('mousemove', function(e) {
    var x = e.pageX - canvas.offsetLeft;
    var y = e.pageY - canvas.offsetTop;
    board.cells.forEach(element => {
        if(element.x < x && x < element.x + element.size && element.y < y && y < element.y + element.size){
            if(element.value == null && !element.leterShown && !board.currentPlayer.cpu)
                element.showLetter(board.currentPlayer.word[board.currentPlayer.turn]);
        }
        else{
            if(element.value == null)
                element.hideLetter();
        }
    });
});


var board = new Board(15);
startGame.addEventListener("click", ()=>{
    /* Game init */
    switch(playerCounter){
        case 0:
            alert("No players added.");
            break;
        case 1:
            alert("Not enought players.")
            break;
        case 2:
            startScreen.style.display = "none";
            gameField.style.display = "block";
            playerOne.cpu.checked ? board.addPlayer(new PlayerCPU(0, PlayerColors[0], playerOne.word.value)) : board.addPlayer(new Player(0, PlayerColors[0], playerOne.word.value));
            playerTwo.cpu.checked ? board.addPlayer(new PlayerCPU(1, PlayerColors[1], playerTwo.word.value)) : board.addPlayer(new Player(1, PlayerColors[1], playerTwo.word.value)); 
            
        break;
        case 3:
            startScreen.style.display = "none";
            gameField.style.display = "block";
            playerOne.cpu.checked ? board.addPlayer(new PlayerCPU(0, PlayerColors[0], playerOne.word.value)) : board.addPlayer(new Player(0, PlayerColors[0], playerOne.word.value));
            playerTwo.cpu.checked ? board.addPlayer(new PlayerCPU(1, PlayerColors[1], playerTwo.word.value)) : board.addPlayer(new Player(1, PlayerColors[1], playerTwo.word.value));
            playerThree.cpu.checked ? board.addPlayer(new PlayerCPU(2, PlayerColors[1], playerThree.word.value)) : board.addPlayer(new Player(1, PlayerColors[2], playerThree.word.value));
            break;
        case 4:
            startScreen.style.display = "none";
            gameField.style.display = "block";
            playerOne.cpu.checked ? board.addPlayer(new PlayerCPU(0, PlayerColors[0], playerOne.word.value)) : board.addPlayer(new Player(0, PlayerColors[0], playerOne.word.value));
            playerTwo.cpu.checked ? board.addPlayer(new PlayerCPU(1, PlayerColors[1], playerTwo.word.value)) : board.addPlayer(new Player(1, PlayerColors[1], playerTwo.word.value));
            playerThree.cpu.checked ? board.addPlayer(new PlayerCPU(2, PlayerColors[2], playerThree.word.value)) : board.addPlayer(new Player(1, PlayerColors[2], playerThree.word.value));
            playerFour.cpu.checked ? board.addPlayer(new PlayerCPU(3, PlayerColors[3], playerFour.word.value)) : board.addPlayer(new Player(1, PlayerColors[3], playerFour.word.value));
            break;
    }
    board.draw();
    board.currentPlayer = board.players[0];
    board.currentPlayer.playTurn(board);
    if(board.currentPlayer.cpu){
        board.currentPlayer.playTurn(board);
    }  
    /* Game init End*/
});

document.addEventListener("DOMContentLoaded", ()=>{
    instructionsOverlay.addEventListener("click", ()=>{
        instructionsOverlay.style.display = "none";
    });
    addPlayerFrontend.addEventListener("click", ()=>{
        if(playerCounter == 0){
            var p1 = new PlayerFrontEnd("One");
            playerOne = p1;
            playerOne.wrapper.style.display = "flex";
            playerCounter++;
        }
        else if(playerCounter == 1){
            var p2 = new PlayerFrontEnd("Two");
            playerTwo = p2;
            playerTwo.wrapper.style.display = "flex";
            playerCounter++;
        }
        else if(playerCounter == 2){
            var p3 = new PlayerFrontEnd("Three");
            playerThree = p3;
            playerThree.wrapper.style.display = "flex";
            playerCounter++;
        }
        else if(playerCounter == 3){
            var p4 = new PlayerFrontEnd("Four");
            playerFour = p4;
            playerFour.wrapper.style.display = "flex";
            playerCounter++;
        }
    });
    removePlayer.addEventListener("click", (PlayerOne)=>{

        if(playerCounter == 1){
            playerOne.wrapper.style.display = "none";
            playerOne.name.value = '';
            playerOne.word.value = '';
            playerCounter--;
        }
        else if(playerCounter == 2){
            playerTwo.wrapper.style.display = "none";
            playerTwo.name.value = '';
            playerTwo.word.value = '';
            playerCounter--;
        }
        else if(playerCounter == 3){
            playerThree.wrapper.style.display = "none";
            playerThree.name.value = '';
            playerThree.word.value = '';
            playerCounter--;
        }
        else if(playerCounter == 4){
            playerFour.wrapper.style.display = "none";
            playerFour.name.value = '';
            playerFour.word.value = '';
            playerCounter--; 
        }
    });
    setValue();
});
range.addEventListener('input', setValue);

function hexToRgbA(hex){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(', ')+', 0.5)';
    }
    throw new Error('Bad Hex');
}
